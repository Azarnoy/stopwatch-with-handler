package com.dmitriy.azarenko.stopwatchwithhandler;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView textOfSeconds;
    private Button startBut;
    private Button stopBut;
    private Button resetBut;
    public static final int updateMes = 1;
    public static final int stopMes = 2;
    int seconds = 60;
    boolean retry;


    Handler h = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (msg.what == updateMes) {
                textOfSeconds.setText(String.valueOf(seconds));
                seconds--;
                h.sendEmptyMessageDelayed(updateMes, 1000);
            } else if (msg.what == stopMes) {
                Toast.makeText(getApplicationContext(), "Finish", Toast.LENGTH_SHORT);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        h.removeMessages(updateMes);
        retry = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (retry) {
            h.sendEmptyMessageDelayed(updateMes, 1000);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        work();

    }

    private void work() {

        textOfSeconds = (TextView) findViewById(R.id.textOfSeconds);
        startBut = (Button) findViewById(R.id.buttonStart);
        stopBut = (Button) findViewById(R.id.buttonStop);
        resetBut = (Button) findViewById(R.id.buttonReset);

        startBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                h.sendEmptyMessageDelayed(updateMes, 1000);
            }
        });


        stopBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                h.removeMessages(updateMes);
                h.sendEmptyMessage(stopMes);

            }
        });

        resetBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                h.removeMessages(updateMes);
                seconds = 60;
                textOfSeconds.setText(String.valueOf(seconds));

            }
        });
    }
}
